﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using A1r.Input;

namespace Towerbeat
{
    //Pair helper
    public class Pair<T1, T2>
    {
        public Pair(T1 f, T2 s) { First = f; Second = s; }
        public T1 First { get; set; }
        public T2 Second { get; set; }
    }
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game : Microsoft.Xna.Framework.Game
    {
        public Game()
        {
            graphics = new GraphicsDeviceManager(this);
            inputManager = new InputManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            this.Components.Add(inputManager);
            base.Initialize();
            camera = new Camera();
            world = new World(GraphicsDevice, camera);
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            GameContent.init(Content);

            GameContent.loadTexture("background");
            GameContent.loadTexture("wolf");
            GameContent.loadTexture("ground");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gametime)
        {
            // TODO: Add your update logic here
            world.Update(gametime, inputManager);
            base.Update(gametime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gametime)
        {
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, null, null, null, null, camera.getTransformation());
            camera.DrawBackground(GraphicsDevice, spriteBatch);
            world.Draw(gametime, spriteBatch);
            spriteBatch.End();
            base.Draw(gametime);
        }

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        InputManager inputManager;
        Camera camera;
        World world;
    }
}
