﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;

namespace Towerbeat
{
    public class Character : GameAnimatedEntity
    {
        public Character(String spriteRes, Vector2 size, String defaultAnim, TimeSpan animDelay) : base(spriteRes, size, defaultAnim, animDelay)
        {

        }

        public override void Update(GameTime gt)
        {
            base.Update(gt);
        }
    }
}
