﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace Towerbeat
{
    public static class GameContent
    {
        public static void init(ContentManager cm)
        {
            content = cm;
            textureDB = new Dictionary<string, Texture2D>();
        }

        public static void loadTexture(String name)
        {
            textureDB.Add(name, content.Load<Texture2D>(name));
        }

        public static Texture2D getTexture(String name)
        {
            return (textureDB[name]);
        }

        static ContentManager content;
        static Dictionary<String, Texture2D> textureDB;
    }
}
