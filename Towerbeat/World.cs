﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections;
using System.Collections.Generic;
using A1r.Input;

namespace Towerbeat
{
    public class World
    {
        public World(GraphicsDevice gd, Camera cam)
        {
            _terrain = new List<WorldChunk>();
            _creatures = new List<Creature>();
            _player = new Player();
            cam.HookTo(_player, new Vector2(-15, -gd.Viewport.Bounds.Height * 0.6f));
            _rnd = new Random();
            _terrain.Add(new WorldChunk(0, 0));
            for (int i = 0; i < 20; i++) GenerateNextChunk();
        }

        public void Update(GameTime gt, InputManager im)
        {
            _player.Update(gt, im);
            if (!_terrain[(int)(_player.GetPosition().X / WorldChunk.tileSize.X)].GravityCollide(_player))
            {
                _player.Move(gravity);
            }
        }

        public void Draw(GameTime gt, SpriteBatch sb)
        {
            for (int i = 0; i < _terrain.Count; i++)
            {
                _terrain[i].Draw(gt, sb);
            }
            _player.Draw(gt, sb);
        }

        void GenerateNextChunk()
        {
            int floorHeight = 0; ;
            int randPercent = _rnd.Next(0, 99);

            if (randPercent >= 0 && randPercent < 10) floorHeight = 0; //No floor
            else if (randPercent >= 10 && randPercent < 30) floorHeight = _terrain[_terrain.Count - 1].GetFloorHeight() - 1; //Lower
            else if (randPercent >= 30 && randPercent < 80) floorHeight = _terrain[_terrain.Count - 1].GetFloorHeight(); //Same
            else if (randPercent >= 80 && randPercent < 100) floorHeight = _terrain[_terrain.Count - 1].GetFloorHeight() + 1; //Higher
            _terrain.Add(new WorldChunk(_terrain.Count, floorHeight));
        }
        //Static constants
        static Vector2 gravity = new Vector2(0, 2);

        List<WorldChunk> _terrain;
        List<Creature> _creatures;
        Player _player;
        Random _rnd;
    }

    public class WorldChunk
    {
        public WorldChunk(int xpos, int fHeight)
        {
            floorHeight = fHeight;
            _ground = new GameEntity("ground", tileSize);
            _ground.Move(new Vector2(xpos * tileSize.X, floorHeight * tileSize.Y));
        }

        public int GetFloorHeight()
        {
            return (floorHeight);
        }

        public bool GravityCollide(GameEntity ent)
        {
            return (_ground.Collides(ent));
        }

        public void Draw(GameTime gt, SpriteBatch sb)
        {
            _ground.Draw(gt, sb);
        }

        //Constants
        public static Vector2 tileSize = new Vector2(50, 50); //px

        int floorHeight;
        GameEntity _ground;
    }
}
