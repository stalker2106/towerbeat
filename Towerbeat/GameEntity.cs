﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;

namespace Towerbeat
{
    public class GameEntity
    {
        public GameEntity()
        {
            baseInit();
        }

        public GameEntity(String spriteRes, Vector2 size)
        {
            baseInit();
            setSprite(spriteRes);
            _size = size;
        }

        public void baseInit()
        {
            _position = Vector2.Zero;
            _color = Color.White;
        }

        public void setSprite(String spriteRes)
        {
            _texture = GameContent.getTexture(spriteRes);
        }

        public void Move(Vector2 vector)
        {
            _position += vector;
        }

        public void Move(Axis direction, float value)
        {
            if (direction == Axis.X) _position.X += value;
            else if (direction == Axis.Y) _position.Y += value;
        }

        public Vector2 GetPosition()
        {
            return (_position);
        }

        public Rectangle GetHitbox()
        {
            return (new Rectangle((int)(_position.X - (_size.X / 2)), (int)(_position.Y - _size.Y), (int)_size.X, (int)_size.Y));
        }

        public bool Collides(GameEntity ent)
        {
            return (GetHitbox().Intersects(ent.GetHitbox()));
        }

        public virtual void Update(GameTime gt)
        {

        }

        public virtual void Draw(GameTime gt, SpriteBatch sb)
        {
            sb.Draw(_texture, _position, null, _color, 0f, new Vector2(_size.X / 2, _size.Y), 1.0f, SpriteEffects.None, 1.0f);
        }

        public enum Axis { X, Y };
        protected Vector2 _position, _size;
        protected Texture2D _texture;
        protected Color _color;
    }

    public class GameAnimatedEntity : GameEntity
    {
        public GameAnimatedEntity(String defaultAnim, TimeSpan animDelay) : base()
        {
            baseAInit(defaultAnim, animDelay);
        }

        public GameAnimatedEntity(String spriteRes, Vector2 size, String defaultAnim, TimeSpan animDelay) : base(spriteRes, size)
        {
           baseAInit(defaultAnim, animDelay);
        }

        public void baseAInit(String defaultAnim, TimeSpan animDelay)
        {
            _animState = new Pair<string, int>(defaultAnim, 0);
            _animDelay = animDelay;
            _animTimer = animDelay;
            _animations = new Dictionary<string, Rectangle[]>();
        }

        public void addAnimation(String name, params Rectangle[] regions)
        {
            _animations.Add(name, regions);
        }

        public void setAnimState(String name, int frame = 0)
        {
            _animState.First = name;
            _animState.Second = frame;
        }

        public override void Update(GameTime gt)
        {
            if (_animations[_animState.First].Length <= 1) return;
            _animTimer -= gt.ElapsedGameTime;
            if (_animTimer.Milliseconds <= 0)
            {
                _animTimer = _animDelay;
                _animState.Second++;
                if (_animState.Second >= _animations[_animState.First].Length) _animState.Second = 0;
            }
        }

        public override void Draw(GameTime gt, SpriteBatch sb)
        {
            sb.Draw(_texture, _position, _animations[_animState.First][_animState.Second], _color, 0f, new Vector2(_size.X / 2, _size.Y), 1.0f, SpriteEffects.None, 1.0f);
        }

        TimeSpan _animDelay, _animTimer;
        Pair<String, int> _animState;
        Dictionary<String, Rectangle[]> _animations;
    }

    public enum Positioning
    {
        TopLeft,
        TopCenter,
        TopRight,
        Left,
        Center,
        Right,
        BottomLeft,
        BottomCenter,
        BottomRight
    }
}
