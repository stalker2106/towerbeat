﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Towerbeat
{
    public class Camera
    {
        public Camera()
        {
            _zoom = 1.0f;
            _rotation = 0f;
            _background = GameContent.getTexture("background");
        }

        public void HookTo(GameEntity ent, Vector2 offset)
        {
            //Move smooth camera to a thing
            _hook = ent;
            _offset = offset;
        }

        protected Vector2 getOrigin2D()
        {
            return (new Vector2((_hook.GetPosition().X + _offset.X), (_hook.GetPosition().Y + _offset.Y)));
        }

        protected Vector3 getOrigin3D()
        {
            return (new Vector3((_hook.GetPosition().X + _offset.X), (_hook.GetPosition().Y + _offset.Y), 0));
        }

        //Returns the player view
        public Matrix getTransformation()
        {
            _transform = Matrix.CreateTranslation(getOrigin3D()) *
                         Matrix.CreateRotationZ(_rotation) *
                         Matrix.CreateScale(new Vector3(_zoom, _zoom, 1));
            return (Matrix.Invert(_transform));
        }

        public void DrawBackground(GraphicsDevice gd, SpriteBatch sb)
        {
            sb.Draw(_background, getOrigin2D(), null, Color.White, 0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0f);
        }

        float _zoom, _rotation;
        GameEntity _hook;
        Vector2 _offset;
        Matrix _transform;
        Texture2D _background;
    }
}
