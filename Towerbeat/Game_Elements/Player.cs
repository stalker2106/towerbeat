﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using A1r.Input;
using System;

namespace Towerbeat
{
    public class Player : Character
    {
        public Player() : base("wolf", new Vector2(64, 32), "stand", new TimeSpan(2500000))
        {
            _position = new Vector2(0, -100);
            addAnimation("stand", new Rectangle(0, 0, 64, 32));
            addAnimation("right", new Rectangle(0, 0, 64, 32), new Rectangle(64, 0, 64, 32), new Rectangle(128, 0, 64, 32), new Rectangle(192, 0, 64, 32), new Rectangle(256, 0, 64, 32));
            addAnimation("left", new Rectangle(0, 32, 64, 32), new Rectangle(64, 32, 64, 32), new Rectangle(128, 32, 64, 32), new Rectangle(192, 32, 64, 32), new Rectangle(256, 32, 64, 32));
        }

        public void Update(GameTime gt, InputManager im)
        {
            base.Update(gt);

            if (im.IsPressed(Keys.Left))
            {
                if (im.JustPressed(Keys.Left)) setAnimState("left");
                Move(Axis.X, -0.1f * gt.ElapsedGameTime.Milliseconds);
            }
            else if (im.IsPressed(Keys.Right))
            {
                if (im.JustPressed(Keys.Right)) setAnimState("right");
                Move(Axis.X, 0.1f * gt.ElapsedGameTime.Milliseconds);
            }
            else if (im.IsPressed(Keys.Space))
            {
                if (im.JustPressed(Keys.Space)) setAnimState("stand");
                Move(Axis.Y, -0.3f * gt.ElapsedGameTime.Milliseconds);
            }
            else
                setAnimState("stand");
        }
    }
}
